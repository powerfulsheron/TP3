// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

//Pour le hash
var crypto = require('crypto'), algorithm = 'aes-256-ctr', password = 'd6F3Efeq';

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
		if(err){console.error(err);}
	});
});

//Mapping de l'url http://localhost:1234/login

app.post("/login", function(req, res, next) {
	// On récupère les variables du formulaire dans la requete
	var login = req.body.login;
	var password = req.body.password;
	// On regarde si présent dans la bd
	db.all("SELECT ident, password FROM users where ident=? and password=?",[login,password], function(err, data) {
		console.log(data);
		// Pas présent, la connection à échoué
		if (data.length === 0){ 
			res.json({status:false});
		}//L'utilisateur est log
		else{
			// On crée un token en utilisant le package JWT, on ajoute un sel
			var jwt = require('jsonwebtoken');
			var cipher = crypto.createCipher(algorithm,password);
			var loginHash = cipher.update(login,'utf8','hex');
			var token = jwt.sign({ user: loginHash }, 'sel');

			// on regarde dans la bd si le couple login/token existe dans sessions
			db.all("SELECT ident, token FROM sessions where ident=? and token=?",[login,token], function(err, data) {
				// Si n'existe pas on insert
				if (data.length === 0){ 
					db.all("INSERT INTO sessions (ident,token) VALUES (?,?)",[login,token], function(err, data){
						if(err){console.error(err);}
					});
				}
			if(err){console.error(err);}
			});
			//Création et envoi du cookie en utilisant le package cookie
			var cookie = require('cookie');
			var escapeHtml = require('escape-html');
			var http = require('http');
			var url = require('url');
			res.setHeader('Set-Cookie', cookie.serialize('CookieTP3', token, {
				httpOnly: true,
				maxAge: 60 * 60 * 24 * 7 // 1 week 
			}));

			res.json({status:true, token:token});
		}
		if(err){console.error(err);}
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});